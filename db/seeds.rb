# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'csv'

pwd = File.dirname(__FILE__)

CSV.foreach("#{pwd}/seeds/departements.csv") do |departement|
  Departement.create(nom: departement[2], code: departement[1])
end

CSV.foreach("#{pwd}/seeds/pollutions.csv", col_sep: ';') do |pollution|
  Pollution.create(code: pollution[0], tonnes: pollution[1])
end

CSV.foreach("#{pwd}/seeds/medecins.csv", col_sep: ';') do |medecins|
  HabitantsParMedecin.create(code: medecins[0], nombre: medecins[1])
end
