class ChangeDepartementsPrimaryKey < ActiveRecord::Migration
  def change
    change_table :departements do |t|
      t.remove :id
      t.remove :code
      t.integer :code, :primary_key
    end
  end
end
