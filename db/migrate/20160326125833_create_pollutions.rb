class CreatePollutions < ActiveRecord::Migration
  def change
    create_table :pollutions, id: false do |t|
      t.string :code
      t.integer :tonnes
    end
    execute "ALTER TABLE pollutions ADD PRIMARY KEY (code);"
  end
end
