class RemoveRegionFromDepartements < ActiveRecord::Migration
  def change
    remove_reference :departements, :region, index: true, foreign_key: true
  end
end
