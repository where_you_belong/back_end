class DestroyBullshits < ActiveRecord::Migration
  def change
    drop_table :bullshits
    drop_table :regions
  end
end
