class RemoveNameFromDepartements < ActiveRecord::Migration
  def change
    change_table :departements do |t|
      t.remove :name
    end
  end
end
