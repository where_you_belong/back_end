class FixDepartements < ActiveRecord::Migration
  def change
    drop_table :departements
    create_table :departements do |t|
      t.string :code
      t.string :name
    end
  end
end
