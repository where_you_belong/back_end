class RebuildDepartements < ActiveRecord::Migration
  def change
    drop_table :departements
    create_table :departements, primary_key: :code do |t|
    end
  end
end
