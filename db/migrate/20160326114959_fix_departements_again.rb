class FixDepartementsAgain < ActiveRecord::Migration
  def change
    change_table :departements do |t|
      t.rename :name, :nom
    end
  end
end
