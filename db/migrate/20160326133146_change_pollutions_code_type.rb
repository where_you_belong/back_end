class ChangePollutionsCodeType < ActiveRecord::Migration
  def change
    change_column :pollutions, :code, :string
  end
end
