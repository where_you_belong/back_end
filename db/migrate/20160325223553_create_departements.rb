class CreateDepartements < ActiveRecord::Migration
  def change
    create_table :departements do |t|
      t.integer :code
      t.references :region, index: true, foreign_key: true
      t.string :name

      t.timestamps null: false
    end
  end
end
