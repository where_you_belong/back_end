# Where you belong - Back End

## System dependencies

```
$ sudo apt-get install libpq-dev postgresql
```

## Installation

```
$ sudo su - postgres
$ psql
=# CREATE ROLE *where_you_belong_user* WITH PASSWORD '*password*' LOGIN CREATEDB;
=# \q
$ exit
$ createdb *where_you_belong_db*

$ git clone git@git.framasoft.org:where_you_belong/back_end.git
$ cd back_end
$ bundle install
```

Set up config/database.yml

```
$ rake secrets
$ rails server
```  

## Tests

```
$ rake
```
