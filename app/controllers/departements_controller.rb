class DepartementsController < ApplicationController
  def index
    params[:facteurs] ||= {}
    @departements = Departement.all
    render json: { departements: JSON.parse(@departements.to_json(score_params: params[:facteurs])) }
  end
end
