class HabitantsParMedecin < ActiveRecord::Base
  belongs_to :departement, primary_key: 'code'

  def self.max
    HabitantsParMedecin.maximum('nombre')
  end
end
