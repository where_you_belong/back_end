class Departement < ActiveRecord::Base
  has_one :pollution, foreign_key: 'code', primary_key: 'code'
  has_one :habitants_par_medecin, foreign_key: 'code', primary_key: 'code'

  def score(params = {})
    facteur_pollution = (params.fetch('0', {'qualite_de_lair'=> 5}).fetch('qualite_de_lair')).to_i
    facteur_soins = (params.fetch('1', {'qualite_des_soins' => 1}).fetch('qualite_des_soins')).to_i

    if pollution
      score_pollution = 1 - ((pollution.tonnes.to_f / Pollution.max))
    else
      return 0
    end

    if habitants_par_medecin
      score_soins = 1 - ((habitants_par_medecin.nombre / HabitantsParMedecin.max))
    else
      return 0
    end

    ((facteur_soins * score_soins + facteur_pollution * score_pollution) * 10 / (facteur_pollution + facteur_soins)).ceil
  end

  def as_json(options = {})
    options.merge!(except: [:created_at, :updated_at])
    json = super(options)
    json['score'] = score(options[:score_params])
    json
  end
end
