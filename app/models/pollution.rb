class Pollution < ActiveRecord::Base
  belongs_to :departement, foreign_key: 'code', primary_key: 'code'

  def self.max
    Pollution.maximum('tonnes')
  end
end
