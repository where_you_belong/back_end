require 'rails_helper'

describe DepartementsController do
  describe 'GET #index' do
    let!(:departement) { create :departement }

    subject { get :index, format: :json }
    it { is_expected.to have_http_status(:ok) }
    it 'returns a score for departements' do
      subject
      result = JSON.parse(response.body)
      expect(result['departements'][0]).to have_key('score')
    end
  end
end
