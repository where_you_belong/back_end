FactoryGirl.define do
  factory :departement do
    sequence(:code) {|n| n }
  end
end
