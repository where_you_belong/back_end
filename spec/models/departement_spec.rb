require 'rails_helper'

RSpec.describe Departement, type: :model do
  it { is_expected.to respond_to(:code) }
  it { is_expected.to respond_to(:score) }
end
